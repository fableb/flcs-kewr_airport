# KEWR (EWR) Newark Liberty International Airport

This is KEWR airport scenery for FlightGear Flight Simulator with

- Day/night lightmaps
- Ground net
- and more to come...

## Installation

- Download the repository .zip file with the link '...' -> 'Download repository'
- Extract the downloaded file into a directory of your choice
- Add this directory to 'Add-ons' -> 'Additional scenery folders' in FlightGear GUI launcher.

## Screenshots

![...](./media/fgfs-20211014034144.png)
![...](./media/fgfs-20211014034338.png)
![...](./media/fgfs-20211014034420.png)
![...](./media/fgfs-20211014034504.png)
![...](./media/fgfs-20211014034556.png)
![...](./media/fgfs-20211014034626.png)


## List of Airports I am Working On:

- MDPP - Gregorio Luperón International Airport, Puerto Plata Airport: https://bitbucket.org/fableb/flcs-mdpp_airport/src/master/
- MDSD - Las Américas International Airport: https://bitbucket.org/fableb/flcs-mdsd_airport/src/master/
- KMIA - Miami International Airport: https://bitbucket.org/fableb/flcs-kmia_airport/src/master/
- KWER - Newark Liberty International Airport: https://bitbucket.org/fableb/flcs-kewr_airport/src/master/
- TFFR - Pointe-à-Pitre International Airport: https://bitbucket.org/fableb/flcs-tffr_airport/src/master/
